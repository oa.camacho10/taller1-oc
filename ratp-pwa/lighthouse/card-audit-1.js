'use strict';

const Audit = require('lighthouse').Audit;

const MAX_TIME = 3000;

class LoadAudit extends Audit {
    static get meta() {
        return {
            category: 'MyPerformance - For 3 seconds',
            name: 'card-audit-1',
            description: 'Schedule card initialized and ready - For 3 seconds',
            failureDescription: 'Schedule Card slow to initialize - For 3 seconds',
            helpText: 'Used to measure time from navigationStart to when the schedule ' +
            ' card is shown. For 3 seconds',

            requiredArtifacts: ['TimeToCard']
        };
    }

    static audit(artifacts) {
        const loadedTime = artifacts.TimeToCard;

        const belowThreshold = loadedTime <= MAX_TIME;

        return {
            rawValue: loadedTime,
            score: belowThreshold
        };
    }
}

module.exports = LoadAudit;